#!/bin/bash

# /!\ This script needs the bc package installed on the server.
# Helper to let you run the install script from anywhere.
currentscriptpath () {
  SOURCE="${BASH_SOURCE[0]}"
  # resolve $SOURCE until the file is no longer a symlink
  while [ -h "$SOURCE" ]; do

    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
  done
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  echo $DIR
}

### Directory webroot & Profile name
DIRECTORY="htdocs"
PROFILE="arena"

### Default URI.
URI="http://domain.fr"

### Working directory.
RESULT=$(currentscriptpath)
SOURCES="$RESULT/../src"
WEBROOT="$RESULT/../$DIRECTORY"

### Make drush a variable to use the one shipped with the repository.
DRUSH="drush -y --root=$ROOT --uri=$URI"

### Install core if it's not
if [ ! -d "$WEBROOT" ]; then
  # Run drush make for installing site
  $DRUSH make project-core.make htdocs -y
  echo "$(tput setaf 2)[ok]$(tput sgr0) Install core project"
fi

### Create profiles directory
if [ ! -d "$WEBROOT/profiles/$PROFILE" ]; then
  mkdir $WEBROOT/profiles/$PROFILE
  cd $WEBROOT/profiles/$PROFILE
  echo "$(tput setaf 2)[ok]$(tput sgr0) Profile directory created"
  
  # Create symmlink profile file
  ln -s $SOURCES/../project.make
  ln -s $SOURCES/profiles/$PROFILE/$PROFILE.info
  ln -s $SOURCES/profiles/$PROFILE/$PROFILE.install
  ln -s $SOURCES/profiles/$PROFILE/$PROFILE.profile
  echo "$(tput setaf 2)[ok]$(tput sgr0) Profile's file symlink"
else
  cd $WEBROOT/profiles/$PROFILE
fi


### Install contrib module
drush -y make --cache-duration-releasexml=300 --concurrency=8 --no-core --contrib-destination=. project.make .
echo "$(tput setaf 2)[ok]$(tput sgr0) Contrib module installed"

### Create drush symlink
if [ -d "$SOURCES/drush" ] && [ ! -d "$WEBROOT/sites/all/drush" ]; then
  cd $WEBROOT/sites/all/
  ln -s $SOURCES/drush
  echo "$(tput setaf 2)[ok]$(tput sgr0) Drush symlink"
fi

### Create custom module symlink
if [ -d "$SOURCES/profiles/$PROFILE/modules/custom" ] && [ ! -d "$WEBROOT/profiles/$PROFILE/modules/custom" ]; then
  cd $WEBROOT/profiles/$PROFILE/modules
  ln -s $SOURCES/profiles/$PROFILE/modules/custom
  echo "$(tput setaf 2)[ok]$(tput sgr0) Custom modules symlink"
fi

### Create custom theme symlink
if [ -d "$SOURCES/profiles/$PROFILE/themes/custom" ] && [ ! -d "$WEBROOT/profiles/$PROFILE/themes/custom" ]; then
  cd $WEBROOT/profiles/$PROFILE/themes
  ln -s $SOURCES/profiles/$PROFILE/themes/custom
  echo "$(tput setaf 2)[ok]$(tput sgr0) Custom themes symlink"
fi

### Create custom features symlink
if [ -d "$SOURCES/profiles/$PROFILE/modules/features" ] && [ ! -d "$WEBROOT/profiles/$PROFILE/modules/features" ]; then
  cd $WEBROOT/profiles/$PROFILE/modules
  ln -s $SOURCES/profiles/$PROFILE/modules/features
  echo "$(tput setaf 2)[ok]$(tput sgr0) Custom features symlink"
fi

### Add settings files
cd $WEBROOT
cp $SOURCES/settings.php sites/default/
cp $SOURCES/settings.local.php sites/default/
echo "$(tput setaf 2)[ok]$(tput sgr0) settings files added"

### Back to ROOT project
cd $SOURCES/..
