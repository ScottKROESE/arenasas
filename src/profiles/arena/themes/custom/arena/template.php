<?php

/**
 * Implements template_preprocess_html().
 */
function arena_preprocess_html(&$variables)
{
    $path = drupal_get_path_alias(current_path());
    if (MainPrivateController::isMySpace()) {
        $variables['classes_array'][] = 'my-space';
    }

    if (MainUser::hasRole('suppliers')) {
        $variables['classes_array'][] = 'supplier';
    }
    if (MainUser::hasRole('internal')) {
        $variables['classes_array'][] = 'internal';
    }
    if (MainUser::hasRole('invite')) {
        $variables['classes_array'][] = 'invite';
    }
    if (MainUser::hasRole('admin')) {
        $variables['classes_array'][] = 'admin';
    }

    $fonts = array(
        array(
            'name' => 'Raleway',
            'url' => 'https://fonts.googleapis.com/css?family=Raleway:800'
        ),
        array(
            'name' => 'OpenSans',
            'url' => 'https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,800,800italic'
        ),
    );

    foreach ($fonts as $font) {
        $element = array(
            '#tag' => 'link',
            '#attributes' => array(
                'href' => $font['url'],
                'rel' => 'stylesheet',
                'type' => 'text/css',
            ),
        );
        drupal_add_html_head($element, $font['name']);
    }
}

/**
 * Implements template_preprocess_page().
 */
function arena_preprocess_page(&$variables)
{
    global $user;
    global $base_url;
    global $base_path;

    if (drupal_is_front_page()) {
        $variables['logo'] = $base_url . $base_path . drupal_get_path('theme', 'arena') . '/logo-home.svg';
    } else if (MainPrivateController::isMySpace()) {
        $variables['front_page'] = '/my-space';
        $variables['logo'] = $base_url . $base_path . drupal_get_path('theme', 'arena') . '/logo-private.svg';
    } else {
        $variables['logo'] = $base_url . $base_path . drupal_get_path('theme', 'arena') . '/logo.svg';
    }
    $variables['footer_logo'] = $base_url . $base_path . drupal_get_path('theme', 'arena') . '/logo-footer.svg';

    $variables['header'] = drupal_get_path('theme', 'arena') . '/templates/header.tpl.php';
    $variables['footer'] = drupal_get_path('theme', 'arena') . '/templates/footer.tpl.php';

    if (!empty($user->roles)) {
        foreach ($user->roles as $role) {
            $filter = '![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s';
            $string_clean = preg_replace($filter, '-', drupal_strtolower($role));

            // looks for page--[role].tpl.php in your theme directory
            // ex: page--administrator.tpl.php
            $variables['theme_hook_suggestions'][] = 'page__' . $string_clean;
        }
    }

    $status = drupal_get_http_header("status");
    if($status == "404 Not Found") {
        $variables['theme_hook_suggestions'][] = 'page__404';
    }
}

/**
 * Implements hook_preprocess_field().
 */
function arena_preprocess_field(&$vars)
{
    if ($vars['element']['#field_name'] == 'field_establishment_date') {
        $old_value = $vars['element']['#items'][0]['value'];
        $new_value = 'Established since ' . $old_value;
        $vars['items'][0]['#markup'] = $new_value;
    }

    if (isset($vars['element']['#items']['0']['format']) && in_array($vars['element']['#items']['0']['format'], array('light_html', 'filtered_html'))) {
        $vars['classes_array'][] = 'wysiwyg';
    }

    if ($vars['element']['#field_name'] == 'field_product_fob_price') {
        $vars['label'] = 'Estimated FOB price';
    }
}


// Add a single suggestion for nodes that have the "Promoted to front page" box checked.
function arena_preprocess_node(&$variables)
{
    $variables['classes_array'][] = 'node-' . $variables['view_mode'];

    $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__view_mode__' . $variables['view_mode'];

    if ($variables['promote']) {
        // looks for node--promoted.tpl.php in your theme directory
        $variables['theme_hook_suggestion'] = 'node__promoted';
    }

    if ($variables['type'] == 'steps') {
        $process_nid = MainPrivateController::getStepParentProcess($variables['nid']);
        $process = node_load($process_nid);

        if ($process->field_process_type[LANGUAGE_NONE][0]['value'] == 0) {
            $variables['classes_array'][] = 'import';
        } elseif ($process->field_process_type[LANGUAGE_NONE][0]['value'] == 1) {
            $variables['classes_array'][] = 'international';
        } elseif ($process->field_process_type[LANGUAGE_NONE][0]['value'] == 2) {
            $variables['classes_array'][] = 'international-portal';
        }
        $step_index = MainPrivateController::getStepIndexFromProcess($variables['nid'], $process_nid);
        $variables['classes_array'][] = 'step-' . $step_index;
    }
    
    if ($variables['type'] == 'supplier_profile') {
        if (!empty($variables['field_arena_supplier'][LANGUAGE_NONE][0]['value'])) {
            $variables['classes_array'][] = 'arena-supplier';
        }
    }
}

/**
 * Implements hook_preprocess_block().
 */
function arena_preprocess_block(&$vars)
{
    // Add current language for language switcher.
    if ($vars['block_html_id'] == 'block-locale-language') {
        global $language;
        $vars['content'] = '<span class="current-lang">' . check_plain($language->native) . '</span>' . $vars['content'];
    }
}

function arena_breadcrumb($variables)
{
    global $base_url;
    global $language;

    $breadcrumb = array();

    if (MainPrivateController::isMySpace()) {
        $breadcrumb[] = array(
            'path' => '/my-space',
            'title' => 'My space',
        );
    }

    if (drupal_is_front_page()) {
        return '';
    } elseif (arg(0) == 'my-space' && arg(1) == 'suppliers-directory' && arg(2)) {
        $breadcrumb[] = array(
            'path' => '/my-space/suppliers-directory',
            'title' => 'Suppliers directory',
        );
        $breadcrumb[] = array(
            'path' => '/my-space/supplier-directory/' . arg(2),
            'title' => drupal_get_title(),
        );
    } elseif (arg(0) == 'node' && is_numeric(arg(1))) {
        $node = node_load(arg(1));
        if ($node->type == 'process') {
            $title = node_page_title($node);
            $path = drupal_get_path_alias('node/' . $node->nid);
            $breadcrumb[] = array(
                'path' => '/' . $path,
                'title' => $title,
            );

            if (strpos(current_path(), 'steps') !== FALSE) {
                $breadcrumb[] = array(
                    'path' => '/' . $path . '/steps',
                    'title' => 'Steps',
                );
            }
        } elseif ($node->type == 'sub_steps') {
            $step_nid = MainPrivateController::getSubstepParentStep($node->nid);
            $step = node_load($step_nid);
            $process_nid = MainPrivateController::getStepParentProcess($step->nid);
            $process = node_load($process_nid);
            $step_index = MainPrivateController::getStepIndexFromProcess($step->nid, $process->nid);

            $process_path_elements = MainPrivateController::pathAuto($process->nid);
            $step_path_elements = MainPrivateController::pathAuto($process->nid, $step_index);
            $sub_step_path_elements = MainPrivateController::pathAuto($process->nid, NULL, $node->nid);
            $path = drupal_get_path_alias('node/' . $process_nid);
            $breadcrumb[] = array(
                'path' => $process_path_elements['path'],
                'title' => $process->title,
            );
            $breadcrumb[] = array(
                'path' => $process_path_elements['path'] . '/steps',
                'title' => 'Steps',
            );
            $breadcrumb[] = array(
                'path' => $step_path_elements['path'] . '#' . $step_path_elements['fragment'],
                'title' => $step->title,
            );
            $breadcrumb[] = array(
                'path' => $sub_step_path_elements['path'],
                'title' => $node->title,
            );
        } elseif ($node->type == 'sub_steps') {
        } elseif ($node->type == 'product') {
            $breadcrumb[] = array(
                'path' => '/my-space/product-catalog',
                'title' => 'Product Catalog',
            );
            $title = node_page_title($node);
            $path = drupal_get_path_alias('node/' . $node->nid);
            $breadcrumb[] = array(
                'path' => '/' . $path,
                'title' => $title,
            );
        } elseif ($node->type == 'page_allies') {
            // Use menu item title and not node title for page_allies
            $menu_tree = menu_get_active_trail();
            if (isset($menu_tree[sizeof($menu_tree) - 1]['title'])) {
                $title = $menu_tree[sizeof($menu_tree) - 1]['title'];
            }
            else  {
                $title = node_page_title($node);
            }
            $path = drupal_get_path_alias('node/' . $node->nid);
            $breadcrumb[] = array(
              'path' => '/' . $path,
              'title' => $title,
            );
        } else {
            $title = node_page_title($node);
            $path = drupal_get_path_alias('node/' . $node->nid);
            $breadcrumb[] = array(
                'path' => '/' . $path,
                'title' => $title,
            );
        }
    } else if (current_path() == 'my-space') {
    } else {
        $title = drupal_get_title();
        $breadcrumb[] = array(
            'path' => '/' . current_path(),
            'title' => $title,
        );
    }

    if (count($breadcrumb) > 0) {
      
        // Get the language prefix from uri not from $language->prefix that 
        // sometimes sucks : see stifer-749
        $uri_chuncks = explode('/',$_SERVER['REQUEST_URI']);
        $languages = language_list($field = 'enabled');
        $language_prefix = '';
        foreach($languages[1] as $l){
          if($uri_chuncks[1] === $l->prefix){
            $language_prefix = $l->prefix;
          }
        }

        array_unshift($breadcrumb, array('path' => '/' . $language_prefix, 'title' => t('Home')));
        $output = '<div class="breadcrumb-container"><div class="breadcrumb">';
        for ($i = 0; $i < count($breadcrumb); $i++) {
            if ($i == count($breadcrumb) - 1) {
                $output .= '<span class="last">' . $breadcrumb[$i]['title'] . '</span>';
            } else {
                $output .= '<a href="' . $base_url . $breadcrumb[$i]['path'] . '">' . $breadcrumb[$i]['title'] . '</a>';
            }
        }
        $output .= '</div></div>';
        return $output;
    }

    return '';
}

function arena_facetapi_link_inactive($variables)
{
    // Sanitizes the link text if necessary.
    $sanitize = empty($variables['options']['html']);
    $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

    $variables['text'] = $link_text . '<span class="chevron">></span>';
    $variables['options']['html'] = TRUE;
    return theme_link($variables);
}

function arena_facetapi_link_active($variables)
{
    // Sanitizes the link text if necessary.
    $sanitize = empty($variables['options']['html']);
    $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

    $variables['text'] = $link_text . '<span class="chevron">></span>';
    $variables['options']['html'] = TRUE;
    return theme_link($variables);
}
