<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
?>
<div class="<?php print $classes; ?> suppliers-infos-details"<?php print $attributes; ?>>
    <?php if (!$label_hidden): ?>
        <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?></div>
    <?php endif; ?>
    <div class="field-items"<?php print $content_attributes; ?>>
        <?php foreach($items as $key => $value): ?>
            <div class="row field-item <?php print $key % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$key]; ?>>
                <?php
                    //$form = drupal_get_form('_main_supplier_quality_control_form');
                    $item = json_decode($value['#markup'], TRUE);
                ?>
                <div class="table-wrapper col-md-12">
                    <div class="table-title">Certification and label</div>
                    <table>
                        <tbody>
                            <?php if(isset($item['process_certification']) && !empty($item['process_certification']['elements'])): ?>
                                <tr>
                                    <td><?php echo $form['process_certification']['#title']; ?></td>
                                    <td>
                                        <?php
                                            $str = '';
                                            foreach ($item['process_certification']['elements'] as $key2 => $value2) {
                                                if ($value2 !== 0) {
                                                    if ($value2 == 'other') {
                                                        echo '<span class="certification_label">'.$value2.': '.$item['process_certification']['please_precise'].'</span>';
                                                    } else {
                                                        echo '<span class="certification_label">'.strtoupper($value2).'</span>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (isset($item['product_certificate']) && !empty($item['product_certificate']['elements'])): ?>
                                <tr>
                                    <td><?php echo $form['product_certificate']['#title']; ?></td>
                                    <td>
                                        <?php
                                            $str = '';
                                            foreach ($item['product_certificate']['elements'] as $key2 => $value2) {
                                                if ($value2 !== 0) {
                                                    if ($value2 == 'other') {
                                                        echo '<span class="certification_label">'.$value2.': '.$item['product_certificate']['please_precise'].'</span>';
                                                    } else {
                                                        echo '<span class="certification_label">'.strtoupper($value2).'</span>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (isset($item['quality_label']) && !empty($item['quality_label']['elements'])): ?>
                                <tr>
                                    <td><?php echo $form['quality_label']['#title']; ?></td>
                                    <td>
                                        <?php
                                            $str = '';
                                            foreach ($item['quality_label']['elements'] as $key2 => $value2) {
                                                if ($value2 !== 0) {
                                                    if ($value2 == 'other') {
                                                        echo '<span class="certification_label">'.$value2.': '.$item['quality_label']['please_precise'].'</span>';
                                                    } else {
                                                        echo '<span class="certification_label">'.strtoupper($value2).'</span>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="table-wrapper col-md-12">
                    <div class="table-title">Quality indicator &amp; Product development and qualification</div>
                    <table class="double-header">
                        <thead>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Quality indicator</td>
                                <td>Product development and qualification</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Own plants</td>
                                <td>
                                    <?php if (isset($item['quality_indicator']['own_plants'])) {
                                        foreach ($item['quality_indicator']['own_plants'] as $key2 => $value2) {
                                            if ($value2['check'] !== 0) {
                                                echo '<span class="quality-indicator">'.$form['quality_indicator']['own_plants'][$key2]['check']['#title'];
                                                if (!empty($value2['additional_information'])) {
                                                    echo '<span class="additional-information">Additional information: '.$value2['additional_information'].'</span>';
                                                }
                                                echo '</span>';
                                            }
                                        }
                                    } ?>
                                </td>
                                <td>
                                    <?php if (isset($item['product_development_and_qualification']['own_plants'])) {
                                        foreach ($item['product_development_and_qualification']['own_plants'] as $key2 => $value2) {
                                            if ($value2['check'] !== 0) {
                                                echo '<span class="quality-indicator">'.$form['product_development_and_qualification']['own_plants'][$key2]['check']['#title'];
                                                if (!empty($value2['additional_information'])) {
                                                    echo '<span class="additional-information">Additional information: '.$value2['additional_information'].'</span>';
                                                }
                                                echo '</span>';
                                            }
                                        }
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Trading activities</td>
                                <td>
                                    <?php if (isset($item['quality_indicator']['trading_activities'])) {
                                        foreach ($item['quality_indicator']['trading_activities'] as $key2 => $value2) {
                                            if ($value2['check'] !== 0) {
                                                echo '<span class="quality-indicator">'.$form['quality_indicator']['trading_activities'][$key2]['check']['#title'];
                                                if (!empty($value2['additional_information'])) {
                                                    echo '<span class="additional-information">Additional information: '.$value2['additional_information'].'</span>';
                                                }
                                                echo '</span>';
                                            }
                                        }
                                    } ?>
                                </td>
                                <td>
                                    <?php if (isset($item['product_development_and_qualification']['trading_activities'])) {
                                        foreach ($item['product_development_and_qualification']['trading_activities'] as $key2 => $value2) {
                                            if ($value2['check'] !== 0) {
                                                echo '<span class="quality-indicator">'.$form['product_development_and_qualification']['trading_activities'][$key2]['check']['#title'];
                                                if (!empty($value2['additional_information'])) {
                                                    echo '<span class="additional-information">Additional information: '.$value2['additional_information'].'</span>';
                                                }
                                                echo '</span>';
                                            }
                                        }
                                    } ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
