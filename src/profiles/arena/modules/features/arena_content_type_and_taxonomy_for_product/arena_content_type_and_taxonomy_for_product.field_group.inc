<?php
/**
 * @file
 * arena_content_type_and_taxonomy_for_product.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function arena_content_type_and_taxonomy_for_product_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_market_manager|node|product|form';
  $field_group->group_name = 'group_market_manager';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Market Manager',
    'weight' => '15',
    'children' => array(
      0 => 'field_product_manager_name',
      1 => 'field_product_manager_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-market-manager field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_market_manager|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_picture|node|product|default';
  $field_group->group_name = 'group_picture';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Picture',
    'weight' => '1',
    'children' => array(
      0 => 'field_product_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Picture',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-picture field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_picture|node|product|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_picture|node|product|full';
  $field_group->group_name = 'group_picture';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Picture',
    'weight' => '1',
    'children' => array(
      0 => 'field_product_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Picture',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-picture field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_picture|node|product|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_specs|node|product|default';
  $field_group->group_name = 'group_specs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Specs',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_product_barcode',
      2 => 'field_product_fob_price',
      3 => 'field_product_annual_sold',
      4 => 'field_product_moq_min_order_qty',
      5 => 'field_product_signboard',
      6 => 'field_product_quality_marks',
      7 => 'title_field',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Specs',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-specs field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_specs|node|product|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_specs|node|product|full';
  $field_group->group_name = 'group_specs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Specs',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_product_barcode',
      2 => 'field_product_fob_price',
      3 => 'field_product_annual_sold',
      4 => 'field_product_moq_min_order_qty',
      5 => 'field_product_signboard',
      6 => 'field_product_quality_marks',
      7 => 'title_field',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Specs',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-specs field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_specs|node|product|full'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Market Manager');
  t('Picture');
  t('Specs');

  return $field_groups;
}
