<?php


function _arena_country_get_zone_list() {
  static $countries_zone;

  if (isset($countries_zone)) {
    return $countries_zone;
  }

  $countries_zone = array(
    'AF' =>
      array(
        'country' => 'Afghanistan',
        'zone' => 'Asia',
      ),
    'AL' =>
      array(
        'country' => 'Albania',
        'zone' => 'Europe (not EU)',
      ),
    'DZ' =>
      array(
        'country' => 'Algeria',
        'zone' => 'Africa',
      ),
    'AD' =>
      array(
        'country' => 'Andorra',
        'zone' => 'Europe (not EU)',
      ),
    'AO' =>
      array(
        'country' => 'Angola',
        'zone' => 'Africa',
      ),
    'AG' =>
      array(
        'country' => 'Antigua and Barbuda',
        'zone' => 'South-America',
      ),
    'AR' =>
      array(
        'country' => 'Argentina',
        'zone' => 'South-America',
      ),
    'AM' =>
      array(
        'country' => 'Armenia',
        'zone' => 'Middle-East',
      ),
    'AU' =>
      array(
        'country' => 'Australia',
        'zone' => 'Oceania',
      ),
    'AT' =>
      array(
        'country' => 'Austria',
        'zone' => 'European Union',
      ),
    'AZ' =>
      array(
        'country' => 'Azerbaijan',
        'zone' => 'Asia ',
      ),
    'BS' =>
      array(
        'country' => 'Bahamas',
        'zone' => 'South-America',
      ),
    'BH' =>
      array(
        'country' => 'Bahrain',
        'zone' => 'Middle-East',
      ),
    'BD' =>
      array(
        'country' => 'Bangladesh',
        'zone' => 'Asia',
      ),
    'BB' =>
      array(
        'country' => 'Barbados',
        'zone' => 'South-America',
      ),
    'BY' =>
      array(
        'country' => 'Belarus',
        'zone' => 'Europe (not EU)',
      ),
    'BE' =>
      array(
        'country' => 'Belgium',
        'zone' => 'European Union',
      ),
    'BZ' =>
      array(
        'country' => 'Belize',
        'zone' => 'South-America',
      ),
    'BJ' =>
      array(
        'country' => 'Benin',
        'zone' => 'Africa',
      ),
    'BT' =>
      array(
        'country' => 'Bhutan',
        'zone' => 'Asia',
      ),
    'BO' =>
      array(
        'country' => 'Bolivia',
        'zone' => 'South-America',
      ),
    'BA' =>
      array(
        'country' => 'Bosnia and Herzegovina',
        'zone' => 'Europe (not EU)',
      ),
    'BW' =>
      array(
        'country' => 'Botswana',
        'zone' => 'Africa',
      ),
    'BR' =>
      array(
        'country' => 'Brazil',
        'zone' => 'South-America',
      ),
    'BN' =>
      array(
        'country' => 'Brunei',
        'zone' => 'Asia',
      ),
    'BG' =>
      array(
        'country' => 'Bulgaria',
        'zone' => 'European Union',
      ),
    'BF' =>
      array(
        'country' => 'Burkina Faso',
        'zone' => 'Africa',
      ),
    'BI' =>
      array(
        'country' => 'Burundi',
        'zone' => 'Africa',
      ),
    'KH' =>
      array(
        'country' => 'Cambodia',
        'zone' => 'Asia',
      ),
    'CM' =>
      array(
        'country' => 'Cameroon',
        'zone' => 'Africa',
      ),
    'CA' =>
      array(
        'country' => 'Canada',
        'zone' => 'North-America',
      ),
    'CV' =>
      array(
        'country' => 'Cape Verde',
        'zone' => 'empty',
      ),
    'CF' =>
      array(
        'country' => 'Central African Republic',
        'zone' => 'Africa',
      ),
    'TD' =>
      array(
        'country' => 'Chad',
        'zone' => 'Africa',
      ),
    'CL' =>
      array(
        'country' => 'Chile',
        'zone' => 'South-America',
      ),
    'CN' =>
      array(
        'country' => 'China',
        'zone' => 'Asia',
      ),
    'CO' =>
      array(
        'country' => 'Colombia',
        'zone' => 'South-America',
      ),
    'KM' =>
      array(
        'country' => 'Comoros',
        'zone' => 'Africa',
      ),
    'CG' =>
      array(
        'country' => 'Congo (Brazzaville)',
        'zone' => 'Africa',
      ),
    'CD' =>
      array(
        'country' => 'Congo (Kinshasa)',
        'zone' => 'Africa',
      ),
    'CR' =>
      array(
        'country' => 'Costa Rica',
        'zone' => 'South-America',
      ),
    'HR' =>
      array(
        'country' => 'Croatia',
        'zone' => 'European Union',
      ),
    'CU' =>
      array(
        'country' => 'Cuba',
        'zone' => 'South-America',
      ),
    'CY' =>
      array(
        'country' => 'Cyprus',
        'zone' => 'European Union',
      ),
    'CZ' =>
      array(
        'country' => 'Czech Republic',
        'zone' => 'European Union',
      ),
    'DK' =>
      array(
        'country' => 'Denmark',
        'zone' => 'European Union',
      ),
    'DJ' =>
      array(
        'country' => 'Djibouti',
        'zone' => 'Africa',
      ),
    'DM' =>
      array(
        'country' => 'Dominica',
        'zone' => 'South-America',
      ),
    'DO' =>
      array(
        'country' => 'Dominican Republic',
        'zone' => 'South-America',
      ),
    'EC' =>
      array(
        'country' => 'Ecuador',
        'zone' => 'South-America',
      ),
    'EG' =>
      array(
        'country' => 'Egypt',
        'zone' => 'Africa',
      ),
    'SV' =>
      array(
        'country' => 'El Salvador',
        'zone' => 'South-America',
      ),
    'GQ' =>
      array(
        'country' => 'Equatorial Guinea',
        'zone' => 'Africa',
      ),
    'ER' =>
      array(
        'country' => 'Eritrea',
        'zone' => 'Africa',
      ),
    'EE' =>
      array(
        'country' => 'Estonia',
        'zone' => 'European Union',
      ),
    'ET' =>
      array(
        'country' => 'Ethiopia',
        'zone' => 'Africa',
      ),
    'FJ' =>
      array(
        'country' => 'Fiji',
        'zone' => 'Oceania',
      ),
    'FI' =>
      array(
        'country' => 'Finland',
        'zone' => 'European Union',
      ),
    'FR' =>
      array(
        'country' => 'France',
        'zone' => 'European Union',
      ),
    'GA' =>
      array(
        'country' => 'Gabon',
        'zone' => 'Africa',
      ),
    'GM' =>
      array(
        'country' => 'Gambia',
        'zone' => 'Africa',
      ),
    'GE' =>
      array(
        'country' => 'Georgia',
        'zone' => 'Asia',
      ),
    'DE' =>
      array(
        'country' => 'Germany',
        'zone' => 'European Union',
      ),
    'GH' =>
      array(
        'country' => 'Ghana',
        'zone' => 'Africa',
      ),
    'GR' =>
      array(
        'country' => 'Greece',
        'zone' => 'European Union',
      ),
    'GD' =>
      array(
        'country' => 'Grenada',
        'zone' => 'South-America',
      ),
    'GT' =>
      array(
        'country' => 'Guatemala',
        'zone' => 'South-America',
      ),
    'GN' =>
      array(
        'country' => 'Guinea',
        'zone' => 'Africa',
      ),
    'GW' =>
      array(
        'country' => 'Guinea-Bissau',
        'zone' => 'Africa',
      ),
    'GY' =>
      array(
        'country' => 'Guyana',
        'zone' => 'South-America',
      ),
    'HT' =>
      array(
        'country' => 'Haiti',
        'zone' => 'South-America',
      ),
    'HN' =>
      array(
        'country' => 'Honduras',
        'zone' => 'South-America',
      ),
    'HU' =>
      array(
        'country' => 'Hungary',
        'zone' => 'European Union',
      ),
    'IS' =>
      array(
        'country' => 'Iceland',
        'zone' => 'Europe (not EU)',
      ),
    'IN' =>
      array(
        'country' => 'India',
        'zone' => 'Asia',
      ),
    'ID' =>
      array(
        'country' => 'Indonesia',
        'zone' => 'Asia',
      ),
    'IR' =>
      array(
        'country' => 'Iran',
        'zone' => 'Asia',
      ),
    'IQ' =>
      array(
        'country' => 'Iraq',
        'zone' => 'Middle-East',
      ),
    'IE' =>
      array(
        'country' => 'Ireland',
        'zone' => 'European Union',
      ),
    'IL' =>
      array(
        'country' => 'Israel',
        'zone' => 'Middle-East',
      ),
    'IT' =>
      array(
        'country' => 'Italy',
        'zone' => 'European Union',
      ),
    'CI' =>
      array(
        'country' => 'Ivory Coast',
        'zone' => 'Africa',
      ),
    'JM' =>
      array(
        'country' => 'Jamaica',
        'zone' => 'South-America',
      ),
    'JP' =>
      array(
        'country' => 'Japan',
        'zone' => 'Asia',
      ),
    'JO' =>
      array(
        'country' => 'Jordan',
        'zone' => 'Middle-East',
      ),
    'KZ' =>
      array(
        'country' => 'Kazakhstan',
        'zone' => 'Asia',
      ),
    'KE' =>
      array(
        'country' => 'Kenya',
        'zone' => 'Africa',
      ),
    'KI' =>
      array(
        'country' => 'Kiribati',
        'zone' => 'Oceania',
      ),
    'KW' =>
      array(
        'country' => 'Kuwait',
        'zone' => 'Middle-East',
      ),
    'KG' =>
      array(
        'country' => 'Kyrgyzstan',
        'zone' => 'Asia',
      ),
    'LA' =>
      array(
        'country' => 'Laos',
        'zone' => 'empty',
      ),
    'LV' =>
      array(
        'country' => 'Latvia',
        'zone' => 'European Union',
      ),
    'LB' =>
      array(
        'country' => 'Lebanon',
        'zone' => 'Middle-East',
      ),
    'LS' =>
      array(
        'country' => 'Lesotho',
        'zone' => 'Africa',
      ),
    'LR' =>
      array(
        'country' => 'Liberia',
        'zone' => 'Africa',
      ),
    'LY' =>
      array(
        'country' => 'Libya',
        'zone' => 'Africa',
      ),
    'LI' =>
      array(
        'country' => 'Liechtenstein',
        'zone' => 'Europe (not EU)',
      ),
    'LT' =>
      array(
        'country' => 'Lithuania',
        'zone' => 'European Union',
      ),
    'LU' =>
      array(
        'country' => 'Luxembourg',
        'zone' => 'European Union',
      ),
    'MK' =>
      array(
        'country' => 'Macedonia',
        'zone' => 'Europe (not EU)',
      ),
    'MG' =>
      array(
        'country' => 'Madagascar',
        'zone' => 'Africa',
      ),
    'MW' =>
      array(
        'country' => 'Malawi',
        'zone' => 'Africa',
      ),
    'MY' =>
      array(
        'country' => 'Malaysia',
        'zone' => 'Asia',
      ),
    'MV' =>
      array(
        'country' => 'Maldives',
        'zone' => 'Asia ',
      ),
    'ML' =>
      array(
        'country' => 'Mali',
        'zone' => 'Africa',
      ),
    'MT' =>
      array(
        'country' => 'Malta',
        'zone' => 'European Union',
      ),
    'MH' =>
      array(
        'country' => 'Marshall Islands',
        'zone' => 'Oceania ',
      ),
    'MR' =>
      array(
        'country' => 'Mauritania',
        'zone' => 'Africa',
      ),
    'MU' =>
      array(
        'country' => 'Mauritius',
        'zone' => 'Africa',
      ),
    'YT' =>
      array(
        'country' => 'Mayotte',
        'zone' => 'empty',
      ),
    'MX' =>
      array(
        'country' => 'Mexico',
        'zone' => 'South-America',
      ),
    'FM' =>
      array(
        'country' => 'Micronesia',
        'zone' => 'Oceania',
      ),
    'MD' =>
      array(
        'country' => 'Moldova',
        'zone' => 'Europe (not EU)',
      ),
    'MC' =>
      array(
        'country' => 'Monaco',
        'zone' => 'Europe (not EU)',
      ),
    'MN' =>
      array(
        'country' => 'Mongolia',
        'zone' => 'Asia ',
      ),
    'ME' =>
      array(
        'country' => 'Montenegro',
        'zone' => 'Europe (not EU)',
      ),
    'MA' =>
      array(
        'country' => 'Morocco',
        'zone' => 'Africa',
      ),
    'MZ' =>
      array(
        'country' => 'Mozambique',
        'zone' => 'Africa',
      ),
    'MM' =>
      array(
        'country' => 'Myanmar',
        'zone' => 'Asia',
      ),
    'NA' =>
      array(
        'country' => 'Namibia',
        'zone' => 'Africa',
      ),
    'NR' =>
      array(
        'country' => 'Nauru',
        'zone' => 'Oceania',
      ),
    'NP' =>
      array(
        'country' => 'Nepal',
        'zone' => 'Asia',
      ),
    'NL' =>
      array(
        'country' => 'Netherlands',
        'zone' => 'European Union',
      ),
    'NZ' =>
      array(
        'country' => 'New Zealand',
        'zone' => 'Oceania',
      ),
    'NI' =>
      array(
        'country' => 'Nicaragua',
        'zone' => 'South-America',
      ),
    'NE' =>
      array(
        'country' => 'Niger',
        'zone' => 'Africa',
      ),
    'NG' =>
      array(
        'country' => 'Nigeria',
        'zone' => 'Africa',
      ),
    'NO' =>
      array(
        'country' => 'Norway',
        'zone' => 'European Union',
      ),
    'OM' =>
      array(
        'country' => 'Oman',
        'zone' => 'Middle-East',
      ),
    'PK' =>
      array(
        'country' => 'Pakistan',
        'zone' => 'Asia',
      ),
    'PW' =>
      array(
        'country' => 'Palau',
        'zone' => 'Oceania',
      ),
    'PA' =>
      array(
        'country' => 'Panama',
        'zone' => 'South-America',
      ),
    'PG' =>
      array(
        'country' => 'Papua New Guinea',
        'zone' => 'Oceania',
      ),
    'PY' =>
      array(
        'country' => 'Paraguay',
        'zone' => 'South-America',
      ),
    'PE' =>
      array(
        'country' => 'Peru',
        'zone' => 'South-America',
      ),
    'PH' =>
      array(
        'country' => 'Philippines',
        'zone' => 'Asia',
      ),
    'PL' =>
      array(
        'country' => 'Poland',
        'zone' => 'European Union',
      ),
    'PT' =>
      array(
        'country' => 'Portugal',
        'zone' => 'European Union',
      ),
    'QA' =>
      array(
        'country' => 'Qatar',
        'zone' => 'Middle-East',
      ),
    'RO' =>
      array(
        'country' => 'Romania',
        'zone' => 'European Union',
      ),
    'RU' =>
      array(
        'country' => 'Russia',
        'zone' => 'Europe (not EU)',
      ),
    'RW' =>
      array(
        'country' => 'Rwanda',
        'zone' => 'Africa',
      ),
    'KN' =>
      array(
        'country' => 'Saint Kitts et Nevis',
        'zone' => 'South-America',
      ),
    'LC' =>
      array(
        'country' => 'Saint Lucia',
        'zone' => 'South-America',
      ),
    'VC' =>
      array(
        'country' => 'Saint Vincent and the Grenadines',
        'zone' => 'South-America',
      ),
    'WS' =>
      array(
        'country' => 'Samoa',
        'zone' => 'Oceania',
      ),
    'SM' =>
      array(
        'country' => 'San Marino',
        'zone' => 'Europe (not EU)',
      ),
    'ST' =>
      array(
        'country' => 'Sao Tome and Principe',
        'zone' => 'empty',
      ),
    'SA' =>
      array(
        'country' => 'Saudi Arabia',
        'zone' => 'Middle-East',
      ),
    'SN' =>
      array(
        'country' => 'Senegal',
        'zone' => 'Africa',
      ),
    'RS' =>
      array(
        'country' => 'Serbia',
        'zone' => 'Europe (not EU)',
      ),
    'SC' =>
      array(
        'country' => 'Seychelles',
        'zone' => 'Africa',
      ),
    'SL' =>
      array(
        'country' => 'Sierra Leone',
        'zone' => 'Africa',
      ),
    'SG' =>
      array(
        'country' => 'Singapore',
        'zone' => 'Asia',
      ),
    'SK' =>
      array(
        'country' => 'Slovakia',
        'zone' => 'European Union',
      ),
    'SI' =>
      array(
        'country' => 'Slovenia',
        'zone' => 'European Union',
      ),
    'SB' =>
      array(
        'country' => 'Solomon Islands',
        'zone' => 'Oceania',
      ),
    'SO' =>
      array(
        'country' => 'Somalia',
        'zone' => 'Africa',
      ),
    'ZA' =>
      array(
        'country' => 'South Africa',
        'zone' => 'Africa',
      ),
    'KR' =>
      array(
        'country' => 'South Korea',
        'zone' => 'Asia',
      ),
    'SS' =>
      array(
        'country' => 'South Sudan',
        'zone' => 'Asia',
      ),
    'ES' =>
      array(
        'country' => 'Spain',
        'zone' => 'European Union',
      ),
    'LK' =>
      array(
        'country' => 'Sri Lanka',
        'zone' => 'Asia',
      ),
    'SD' =>
      array(
        'country' => 'Sudan',
        'zone' => 'Africa',
      ),
    'SR' =>
      array(
        'country' => 'Suriname',
        'zone' => 'South-America',
      ),
    'SZ' =>
      array(
        'country' => 'Swaziland',
        'zone' => 'Africa',
      ),
    'SE' =>
      array(
        'country' => 'Sweden',
        'zone' => 'European Union',
      ),
    'CH' =>
      array(
        'country' => 'Switzerland',
        'zone' => 'Europe (not EU)',
      ),
    'SY' =>
      array(
        'country' => 'Syria',
        'zone' => 'Middle-East',
      ),
    'TJ' =>
      array(
        'country' => 'Tajikistan',
        'zone' => 'Asia',
      ),
    'TZ' =>
      array(
        'country' => 'Tanzania',
        'zone' => 'Africa',
      ),
    'TH' =>
      array(
        'country' => 'Thailand',
        'zone' => 'Asia',
      ),
    'TL' =>
      array(
        'country' => 'Timor-Leste',
        'zone' => 'Asia',
      ),
    'TG' =>
      array(
        'country' => 'Togo',
        'zone' => 'Africa',
      ),
    'TO' =>
      array(
        'country' => 'Tonga',
        'zone' => 'Oceania',
      ),
    'TT' =>
      array(
        'country' => 'Trinidad and Tobago',
        'zone' => 'South-America',
      ),
    'TN' =>
      array(
        'country' => 'Tunisia',
        'zone' => 'Africa',
      ),
    'TR' =>
      array(
        'country' => 'Turkey',
        'zone' => 'Middle-East',
      ),
    'TM' =>
      array(
        'country' => 'Turkmenistan',
        'zone' => 'Asia',
      ),
    'TV' =>
      array(
        'country' => 'Tuvalu',
        'zone' => 'Oceania',
      ),
    'UG' =>
      array(
        'country' => 'Uganda',
        'zone' => 'Africa',
      ),
    'UA' =>
      array(
        'country' => 'Ukraine',
        'zone' => 'Europe (not EU)',
      ),
    'AE' =>
      array(
        'country' => 'United Arab Emirates',
        'zone' => 'Middle-East',
      ),
    'GB' =>
      array(
        'country' => 'United Kingdom',
        'zone' => 'European Union',
      ),
    'US' =>
      array(
        'country' => 'United States',
        'zone' => 'North-America',
      ),
    'UY' =>
      array(
        'country' => 'Uruguay',
        'zone' => 'South-America',
      ),
    'UZ' =>
      array(
        'country' => 'Uzbekistan',
        'zone' => 'Asia',
      ),
    'VU' =>
      array(
        'country' => 'Vanuatu',
        'zone' => 'Oceania',
      ),
    'VE' =>
      array(
        'country' => 'Venezuela',
        'zone' => 'South-America',
      ),
    'VN' =>
      array(
        'country' => 'Vietnam',
        'zone' => 'Asia',
      ),
    'YE' =>
      array(
        'country' => 'Yemen',
        'zone' => 'Middle-East',
      ),
    'ZM' =>
      array(
        'country' => 'Zambia',
        'zone' => 'Africa',
      ),
    'ZW' =>
      array(
        'country' => 'Zimbabwe',
        'zone' => 'Africa',
      ),
  );

  return $countries_zone;
}