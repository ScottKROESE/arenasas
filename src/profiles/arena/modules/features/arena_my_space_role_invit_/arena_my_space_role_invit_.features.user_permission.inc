<?php
/**
 * @file
 * arena_my_space_role_invit_.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function arena_my_space_role_invit__user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access my all space '.
  $permissions['access my all space '] = array(
    'name' => 'access my all space ',
    'roles' => array(
      'internal' => 'internal',
    ),
    'module' => 'main_private',
  );

  // Exported permission: 'access my space'.
  $permissions['access my space'] = array(
    'name' => 'access my space',
    'roles' => array(
      'admin' => 'admin',
      'internal' => 'internal',
      'invite' => 'invite',
      'super admin' => 'super admin',
    ),
    'module' => 'main_private',
  );

  return $permissions;
}
