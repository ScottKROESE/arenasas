<?php
/**
 * @file
 * arena_my_space_role_invit_.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function arena_my_space_role_invit__user_default_roles() {
  $roles = array();

  // Exported role: invite.
  $roles['invite'] = array(
    'name' => 'invite',
    'weight' => 2,
  );

  return $roles;
}
