<?php

/**
 * This abstract class contains basic informations for classes
 */
abstract class arenaUserMigration extends Migration {

    /**
     * Construct method.
     */
    public function __construct($arguments) {
        parent::__construct($arguments);
        $this->team = array(
          new MigrateTeamMember('Quentin', 'quentin.labadens@buzzaka.com', t('Implementor')),
        );
    }
}

/*
 * Migrate product
 */
class accountMigration extends arenaUserMigration {

    /**
     * Implements __contruct() method.
     */
    public function __construct($arguments) {
        parent::__construct($arguments);

        $this->description = t('Migrate account from the CSV source file');

        $columns = array(
          0 => array('first_name', t('First name')),
          1 => array('last_name', t('Last name')),
          2 => array('company', t('Company')),
          3 => array('mail', t('Email')),
        );

        $csv_file = DRUPAL_ROOT . base_path() . '../arena-data-for-import/account.csv';
        if (!file_exists($csv_file)) {
            drupal_set_message(t('File not found : @filename', array('@filename' => $csv_file)), 'error');
        }

        $this->source = new MigrateSourceCSV(
          $csv_file,
          $columns,
          array(
            'delimiter' => ';',
            'header_rows' => 1,
            'embedded_newlines' => TRUE
          )
        );

        $this->map = new MigrateSQLMap($this->machineName,
          array(
            'mail' => array(
              'type' => 'varchar',
              'length' => 255,
              'not null' => TRUE,
            ),
          ),
          MigrateDestinationUser::getKeySchema()
        );

        $options = array();
        $this->destination = new MigrateDestinationUser($options);

        $this->addFieldMapping('mail', 'mail')->description(t('Email'));
        $this->addFieldMapping('name', 'mail')->description(t('Name'));
        $this->addFieldMapping('status')->defaultValue(1);
        $this->addFieldMapping('field_first_name', 'first_name')->description(t('First name'));
        $this->addFieldMapping('field_last_name', 'last_name')->description(t('Last name'));
        $this->addFieldMapping('field_company', 'company')->description(t('Company'));
        $this->addFieldMapping('field_company:create_term')->defaultValue(TRUE);
        $this->addFieldMapping('field_company:ignore_case')->defaultValue(TRUE);
        $this->addFieldMapping('roles')->defaultValue(array(2, 4));
    }

    /**
     * Implements prepareRow() method.
     * @param $row
     * @return bool
     */
    public function prepareRow($row) {
        return TRUE;
    }


}
