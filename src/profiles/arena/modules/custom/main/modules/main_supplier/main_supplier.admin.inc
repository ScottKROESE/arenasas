<?php

function _main_supplier_emails_admin_form() {
    $form = array();

    $form['main_supplier_profile_submitted_mail'] = array(
        '#type' => 'fieldset',
        '#title' => 'Profile submitted email',
    );
    $form['main_supplier_profile_submitted_mail']['main_supplier_profile_submitted_mail_subject'] = array(
        '#title' => 'Subject',
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => variable_get('main_supplier_profile_submitted_mail_subject', ''),
    );
    $form['main_supplier_profile_submitted_mail']['main_supplier_profile_submitted_mail_body'] = array(
        '#title' => 'Body',
        '#type' => 'textarea',
        '#required' => TRUE,
        '#default_value' => variable_get('main_supplier_profile_submitted_mail_body', ''),
    );

    $form['main_supplier_profile_awaiting_validation_mail'] = array(
        '#type' => 'fieldset',
        '#title' => 'Profile awaiting validation email',
    );
    $form['main_supplier_profile_awaiting_validation_mail']['main_supplier_profile_awaiting_validation_mail_subject'] = array(
        '#title' => 'Subject',
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => variable_get('main_supplier_profile_awaiting_validation_mail_subject', ''),
    );
    $form['main_supplier_profile_awaiting_validation_mail']['main_supplier_profile_awaiting_validation_mail_body'] = array(
        '#title' => 'Body',
        '#type' => 'textarea',
        '#required' => TRUE,
        '#default_value' => variable_get('main_supplier_profile_awaiting_validation_mail_body', ''),
    );

    $form['main_supplier_profile_edited_mail'] = array(
        '#type' => 'fieldset',
        '#title' => 'Profile edited email',
    );
    $form['main_supplier_profile_edited_mail']['main_supplier_profile_edited_mail_subject'] = array(
        '#title' => 'Subject',
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => variable_get('main_supplier_profile_edited_mail_subject', ''),
    );
    $form['main_supplier_profile_edited_mail']['main_supplier_profile_edited_mail_body'] = array(
        '#title' => 'Body',
        '#type' => 'textarea',
        '#required' => TRUE,
        '#default_value' => variable_get('main_supplier_profile_edited_mail_body', ''),
    );

    return system_settings_form($form);
}
