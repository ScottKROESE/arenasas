<div class="international-process">
    <div class="description">
        <?php echo $description ?>
    </div>
    <div class="best-practices field-name-field-best-practices">
        <div class="field-label">Best practices:</div>
        <div class="content field-items">
            <?php echo $best_practices ?>
        </div>
    </div>
    <div class="to-be-avoided field-name-field-to-be-avoided">
        <div class="field-label">To be avoided:</div>
        <div class="content field-items">
            <?php echo $to_be_avoided ?>
        </div>
    </div>
    <div class="steps">
        <div class="step-container">
            <div class="step step-1">
<!--                <div class="step-index">Step 1</div>-->
                <div class="step-title"><?php echo $step1_title ?></div>
                <div class="short-description"><?php echo $step1_short_description ?></div>
            </div>
            <div class="links"></div>
        </div>
        <div class="step-container">
            <div class="step step-2">
<!--                <div class="step-index">Step 2</div>-->
                <div class="step-title"><?php echo $step2_title ?></div>
                <div class="short-description"><?php echo $step2_short_description ?></div>
            </div>
            <div class="links">
                <a href="<?php echo $step2_method_1_href ?>" class="arrow method link-1">Method 3NETS</a>
                <a href="<?php echo $step2_method_2_href ?>" class="arrow method link-2">Method AGA</a>
            </div>
        </div>
    </div>
</div>
