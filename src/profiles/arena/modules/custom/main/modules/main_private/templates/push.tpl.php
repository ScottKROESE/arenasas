<div class="push <?php echo $class ?>">
    <h2 class="title"><?php echo $title ?></h2>
    <div class="push-content">
        <p>
            <?php echo $text ?>
        </p>
        <?php echo $link ?>
    </div>
</div>
