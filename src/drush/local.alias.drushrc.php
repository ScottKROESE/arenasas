<?php

$options = array(
  'parent' => '@self',
  'uri' => 'arena-refonte.local.buzzaka.net',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
    '%private_files' => '../arena-private-files/',
  ),
);
