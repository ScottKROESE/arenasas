<?php

$options['shell-aliases'] = array(
    'pull-db' => '!drush sql-drop && drush sql-sync @dev @self',
    'pull-files' => '!drush rsync @dev:%files/ @local:%files --mode=rltDv',
    'pull-private-files' => '!drush rsync @dev:%private_files/ @local:%private_files --mode=rltDv',
);
