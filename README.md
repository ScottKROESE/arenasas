#SOURCE CODES DATING FROM AUGUST 29TH; NEWER CODES EXIST

## A.R.E.N.A. sas Preprod (195.46.203.216)

### Mail in Preprod Environement

**Emails from the preprod are not sent they are stored in a file.**

http://195.46.203.216/sites/default/sendmails/
or
http://developmentsite.alliance-arena.com/sites/default/sendmails/

The Devel module must be activated for it to work.
On the preprod server, Fabernovel has added the following to the file **settings.local.php**:

```
#!php
  // Use Devel's maillog
  $conf['mail_system'] = array( 
    'default-system' => 'DevelMailLog',
  );
  // To set custom path 
  $conf['devel_debug_mail_directory'] = 'sites/default/sendmails';
```